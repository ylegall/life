package org.ygl;

import java.util.Arrays;

import org.apache.log4j.Logger;

/**
 * 
 * @author ylegall
 *
 */
public class LifeGrid {
	
	public static final Logger logger = Logger.getLogger(LifeGrid.class);
	
	public static final byte STATE_DEAD = 0;
	public static final byte STATE_LIVE = 1;
	
	private byte[][] grid1, grid2;
	private byte[][] current, next;
	private long ticks;
	private boolean wrap;
	
	/**
	 * 
	 * @param width
	 * @param height
	 */
	public LifeGrid(int width, int height) {
		if (width < 0 || height < 0) {
			throw new IllegalArgumentException("grid dimensions must be positive");
		}
		logger.info("creating LifeGrid of size: " + height + " x " + width);
		grid1 = new byte[height][width];
		grid2 = new byte[height][width];
		current = grid1;
		next = grid2;
		ticks = 0;
	}
	
	/**
	 * 
	 * @return
	 */
	public byte[][] getCells() {
		return current;
	}
	
	public void toggle(int i, int j) {
		next[i][j] = current[i][j] = (byte)((current[i][j] + 1) % 2);
	}
	
	/**
	 * 
	 * @return
	 */
	public long getTicks() {
		return ticks;
	}
	
	/**
	 * sets the wrap mode.
	 * @param isWrap
	 */
	public void setWrap(boolean isWrap) {
		this.wrap = isWrap;
	}
	
	public void tick() {
		for (int i = 0; i < current.length; i++) {
			for (int j = 0; j < current.length; j++) {
				next[i][j] = STATE_DEAD;
				int neighbors = getNumberOfLivingNeighbors(i, j);
				switch (neighbors) {
				
				// fewer than 2, live cells die, dead cells remain dead:
				case 0:
				case 1:
					next[i][j] = STATE_DEAD;
					break;
					
				case 2:
					next[i][j] = current[i][j];
					break;
					
				// exactly 3, dead cells come to life:
				// alive cells stay alive:
				case 3:
					next[i][j] = STATE_LIVE;
					break;
					
				// Any live cell with more than 3 live neighbors dies.
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
					next[i][j] = STATE_DEAD;
					break;
				default:
					throw new RuntimeException("impossible number of neighbors: " + neighbors);
				}
			}	
		}
		
		ticks++;
		swapBuffers();
	}

	private void swapBuffers() {
		// swap the buffers:
		byte[][] temp = current;
		current = next;
		next = temp;
	}

	private int getNumberOfLivingNeighbors(int i, int j) {
		int numAlive = 0;
		if (isAlive(i-1, j-1)) numAlive++;
		if (isAlive(i-1, j)) numAlive++;
		if (isAlive(i-1, j+1)) numAlive++;
		if (isAlive(i, j-1)) numAlive++;
		if (isAlive(i, j+1)) numAlive++;
		if (isAlive(i+1, j-1)) numAlive++;
		if (isAlive(i+1, j)) numAlive++;
		if (isAlive(i+1, j+1)) numAlive++;
		return numAlive;
	}
	
	private boolean isAlive(int i, int j) {
		if (wrap) {
			if (i == -1) i = current.length-1;
			else if (i == current.length) i = 0;
			
			if (j == -1) j = current.length-1;
			else if (j == current[i].length) j = 0;
		}

		if (i >= 0 && i < current.length) {
			if (j >= 0 && j < current[i].length) {
				return current[i][j] == STATE_LIVE;
			}
		}
		return false;
	}
	
	/**
	 * 
	 */
	public void reset() {
		for (int i = 0; i < grid1.length; ++i) {
			Arrays.fill(current[i], 0, current[i].length, STATE_DEAD);
		}
		ticks = 0;
	}
}
