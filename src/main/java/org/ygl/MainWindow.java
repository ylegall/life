package org.ygl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.KeyStroke;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.log4j.Logger;

public class MainWindow extends MouseAdapter {

	private static final Logger logger = Logger.getLogger(MainWindow.class);
	
	public static final int CELL_SIZE = 8;
	public static final int CANVAS_SIZE = 512;
	
	private JFrame frmLife;
	private JButton btnStart;
	private JPanel canvas;
	private LifeGrid lifeGrid;
	private boolean isRunning = false;
	
	private KeyHandler keyHandler;
	private Timer timer;
	
	private JSlider speedSlider;
	private JPanel sliderPanel;
	private JPanel buttonPanel;
	private JLabel lblSlower;
	private JLabel lblFaster;
	
	private boolean isDebug = false;
	private Point lastDragPoint = new Point();
	
	// settings
	Color fgColor;
	Color bgColor;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frmLife.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		keyHandler = new KeyHandler();
		bgColor = Color.darkGray;
		// bgColor = new Color(0.2f, 0.2f, 0.2f, 0.5f);
		fgColor = Color.orange;
		timer = new Timer(200, new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tick();
			}
		});
		
		initialize();
		lifeGrid = new LifeGrid(canvas.getHeight()/CELL_SIZE, canvas.getHeight()/CELL_SIZE);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		logger.info("initializing main window");
		frmLife = new JFrame();
		frmLife.setTitle("Life - paused");
		frmLife.setBounds(100, 100, 512, 512);
		frmLife.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		canvas = new JPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				draw(g);
			}
		};
		canvas.setBackground(Color.DARK_GRAY);
		canvas.addMouseListener(this);
		canvas.addMouseMotionListener(this);
		canvas.setDoubleBuffered(true);
		canvas.setPreferredSize(new Dimension(CANVAS_SIZE, CANVAS_SIZE));
		
		frmLife.getContentPane().add(canvas, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		frmLife.getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new GridLayout(2, 1, 0, 0));
		
		buttonPanel = new JPanel();
		buttonPanel.setBorder(null);
		panel.add(buttonPanel);
		
		btnStart = new JButton("Start");
		buttonPanel.add(btnStart);
		
		JButton btnClear = new JButton("Reset");
		buttonPanel.add(btnClear);
		
		JButton btnSettings = new JButton("settings");
		buttonPanel.add(btnSettings);
		btnSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SettingsDialog sd = new SettingsDialog();
				sd.setMainWindow(MainWindow.this);
				sd.setVisible(true);
			}
		});
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("Reset button pressed.");
				lifeGrid.reset();
				canvas.repaint();
			}
		});
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				isRunning = !isRunning;
				if (isRunning) {
					btnStart.setText("Stop");
					frmLife.setTitle("Life");
					timer.restart();
				} else {
					pause();
				}
			}
		});
		
		sliderPanel = new JPanel();
		sliderPanel.setBorder(null);
		panel.add(sliderPanel);
		
		lblSlower = new JLabel("slower");
		sliderPanel.add(lblSlower);
		
		speedSlider = new JSlider(20, 1000, 200);
		speedSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				updateSpeed(speedSlider.getValue());
			}
		});
		sliderPanel.add(speedSlider);
		
		lblFaster = new JLabel("faster");
		sliderPanel.add(lblFaster);
		frmLife.pack();

		// prevent spacebar from invoking button clicks:
		InputMap m = (InputMap) UIManager.get("Button.focusInputMap");
		m.put(KeyStroke.getKeyStroke("pressed SPACE"), "none");
		m.put(KeyStroke.getKeyStroke("released SPACE"), "none");
		
		InputMap im = canvas.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = canvas.getActionMap();
		im.put(KeyStroke.getKeyStroke("SPACE"), "spacePressed");
		am.put("spacePressed", keyHandler);
		im.put(KeyStroke.getKeyStroke('`'), "debug");
		am.put("debug", keyHandler);
	}
	
	private void updateSpeed(int speed) {
		timer.setDelay(speed);
	}
	
	private void tick() {
		lifeGrid.tick();
		canvas.repaint();
	}
	
	private void draw(Graphics g) {
		// logger.info("draw()");
		g.setColor(bgColor);
		g.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
		g.setColor(fgColor);
		byte[][] current = lifeGrid.getCells();
		for (int i = 0; i < current.length; ++i) {
			for (int j = 0; j < current[i].length; ++j) {
				if (current[i][j] == LifeGrid.STATE_LIVE) {
					g.fillRect(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE, CELL_SIZE);
				}
			}
		}
		
		if (isDebug) {
			g.drawString("generation: "  + lifeGrid.getTicks(), 10, 10);
		}
	}
	
	private int getClickX(Point p) {
		return p.x / CELL_SIZE;
	}
	
	private int getClickY(Point p) {
		return p.y / CELL_SIZE;
	}
	
	public LifeGrid getLifeGrid() {
		return this.lifeGrid;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// logger.info("mouse clicked: " + e.getX() + "," + e.getY());
		super.mouseClicked(e);
		Point p = e.getPoint();
		int x = getClickX(p);
		int y = getClickY(p);
		lifeGrid.toggle(y, x);
		canvas.repaint();
	}

	
	@Override
	public void mouseDragged(MouseEvent e) {
		super.mouseDragged(e);
		Point p = e.getPoint();
		int x = getClickX(p);
		int y = getClickY(p);
		if (lastDragPoint.x != x || lastDragPoint.y != y) {
			lifeGrid.toggle(y, x);
			lastDragPoint.x = x;
			lastDragPoint.y = y;
			canvas.repaint();
		}
	}

	private void pause() {
		btnStart.setText("Start");
		frmLife.setTitle("Life - paused");
		isRunning = false;
		timer.stop();
	}
	
	private void toggleDebug() {
		isDebug = !isDebug;
		canvas.repaint();
	}
	
	void repaint() {
		canvas.repaint();
	}
	
	@SuppressWarnings("serial")
	public class KeyHandler extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			String action = e.getActionCommand();
			if (action.equals(" ")) {
				spacePressed();
			} else if (action.equals("`")) {
				toggleDebug();
			}
		}

		private void spacePressed() {
			if (isRunning) {
				pause();
			} else {
				tick();
			}
		}
	}

}


