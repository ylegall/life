package org.ygl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.log4j.Logger;

public class SettingsDialog extends JDialog implements ChangeListener {

	private static final long serialVersionUID = -1142567544979783785L;
	
	private static final Logger logger = Logger.getLogger(SettingsDialog.class);
	private final JPanel contentPanel = new JPanel();
	private MainWindow window;

	/**
	 * Create the dialog.
	 */
	public SettingsDialog() {
		setTitle("settings");
		setBounds(100, 100, 304, 213);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel);
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			{
				JCheckBox wrapCheckbox = new JCheckBox("wrap edges");
				wrapCheckbox.addItemListener(new ItemListener() {
					public void itemStateChanged(ItemEvent e) {
						window.getLifeGrid().setWrap(((JCheckBox)e.getSource()).isSelected());
					}
				});
				panel.add(wrapCheckbox);
			}
			{
				Component verticalStrut = Box.createVerticalStrut(20);
				panel.add(verticalStrut);
			}
			{
				JButton fgButton = new JButton("foreground");
				fgButton.setBackground(Color.ORANGE);
				fgButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						final JButton button = (JButton)e.getSource();
						Color newColor = JColorChooser.showDialog(SettingsDialog.this, "foreground color", button.getBackground());
						button.setBackground(newColor);
						window.fgColor = newColor;
						window.repaint();
					}
				});
				panel.add(fgButton);
			}
			{
				Component verticalStrut = Box.createVerticalStrut(20);
				panel.add(verticalStrut);
			}
			{
				JButton bgButton = new JButton("background");
				bgButton.setBackground(Color.DARK_GRAY);
				bgButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						final JButton button = (JButton)e.getSource();
						Color newColor = JColorChooser.showDialog(SettingsDialog.this, "background color", button.getBackground());
						button.setBackground(newColor);
						window.bgColor = newColor;
						window.repaint();
					}
				});
				panel.add(bgButton);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						SettingsDialog.this.dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						SettingsDialog.this.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		//this.pack();
	}
	
	/**
	 * 
	 * @param mw
	 */
	public void setMainWindow(MainWindow mw) {
		this.window = mw;
	}

	public void stateChanged(ChangeEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
